import { Dispatch } from 'redux';
import { connect } from 'react-redux'
import Main from '../components/pages/main/Main';
import IGlobalState from '../state/globalState'
import { MainActions } from '../actions/MainActions';

const mapStateToProps = (state: IGlobalState) => ({
    recipes: state.recipes
})


const mapDispatchToProps = (dispatch: Dispatch) => ({
    onFilterClick: (searchWord: string, searchTag: string) => {
        dispatch({ type: MainActions.MAIN_FILTER, searchWord: searchWord, searchTag: searchTag })
    },
    onResetClick: () => {
        dispatch({ type: MainActions.MAIN_RESET })
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(Main);