import { connect } from 'react-redux'
import IGlobalState from '../state/globalState'
import Filter from '../components/pages/main/Filter';


const mapStateToProps = (state: IGlobalState) => ({
    searchWord: state.searchWord,
    searchTag: state.searchTag,
})



export default connect(mapStateToProps)(Filter);