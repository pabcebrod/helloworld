import { Dispatch } from 'redux';
import { connect } from 'react-redux'

import IGlobalState from '../state/globalState'
import { IRecipe } from '../assets/MockObjects';
import { RecipeDetailsAction } from '../actions/RecipeDetailsActions';
import Recipe from '../components/pages/main/Recipe';

const mapStateToProps = (state: IGlobalState) => ({
    recipes: state.recipes
})


const mapDispatchToProps = (dispatch: Dispatch) => ({
    showRecipeDetails: (recipe: IRecipe) => {
        dispatch({ type: RecipeDetailsAction.SHOW_RECIPE, recipe: recipe })
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(Recipe);