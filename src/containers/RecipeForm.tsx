import { Dispatch } from 'redux';
import { connect } from 'react-redux'
import IGlobalState from '../state/globalState'
import { IRecipe, tags } from '../assets/MockObjects';
import RecipeForm from '../components/pages/create-recipe/RecipeForm';
import { RecipeFormActions } from '../actions/RecipeFormActions';

const mapStateToProps = (state: IGlobalState) => ({
    tags: tags,
})


const mapDispatchToProps = (dispatch: Dispatch) => ({
    addRecipe: (recipe: IRecipe) => {
        dispatch({ type: RecipeFormActions.CREATE_RECIPE, recipe: recipe })
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(RecipeForm);