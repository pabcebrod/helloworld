import * as React from 'react';
import './assets/scss/App.scss';
import Header from './components/Header'
import IGlobalState, { initialState } from './state/globalState';
import { tags, recipeArray2, filterList, IRecipe } from './assets/MockObjects'
import { Provider } from 'react-redux';
import { Action, createStore } from 'redux';
import { BrowserRouter as Router, Redirect, Route, Switch } from "react-router-dom";
import Main from './containers/Main';
import Footer from './components/Footer';
import { IFilterAction, MainActions } from './actions/MainActions'
import Contact from './components/pages/contact/Contact';
import RecipeDetails from './components/pages/show-recipe/recipeDetail';
import AboutUs from './components/pages/about/about';
import { IRecipeFormAction, RecipeFormActions } from './actions/RecipeFormActions';
import RecipeForm from './containers/RecipeForm';
import { IRecipeDetailsAction, RecipeDetailsAction } from './actions/RecipeDetailsActions';

const reducer = (state: IGlobalState = initialState, action: Action) => {
  switch (action.type) {
    case MainActions.MAIN_FILTER:
      const filterAction = action as IFilterAction;
      //Filtrar en recipeArray2
      let filteredList: IRecipe[] = filterList(state.recipesBackup, filterAction.searchWord, filterAction.searchTag)
      return { ...state, recipes: filteredList, searchWord: filterAction.searchWord, searchTag: filterAction.searchTag }

    case MainActions.MAIN_RESET:
      return { ...state, recipes: state.recipesBackup }

    case RecipeFormActions.CREATE_RECIPE:
      const recipeFormAction = action as IRecipeFormAction;

      const newRecipes = state.recipes;
      newRecipes.unshift(recipeFormAction.recipe);

      const newRecipesBackup = state.recipesBackup;
      newRecipesBackup.unshift(recipeFormAction.recipe);
      //Filtrar en recipeArray2
      return { ...state, recipes: newRecipes, newRecipesBackup: newRecipesBackup }

    case RecipeDetailsAction.SHOW_RECIPE:
      const recipeDetailsAction = action as IRecipeDetailsAction;
      //Filtrar en recipeArray2
      return { ...state, recipeDetailed: recipeDetailsAction.recipe }

  }
  return state;
}
const store = createStore(reducer, initialState);

class App extends React.Component {
  public render() {
    return (
      <Provider store={store}>
        <Router>
          <div className="App">
            <Header />
            <Switch>
              <Route path="/contact">
                <Contact />
              </Route>
              <Route path="/about">
                <AboutUs />
              </Route>
              <Route path="/recipe/list">
                <Main />
              </Route>
              <Route path="/recipe/create">
                <RecipeForm />
              </Route>
              <Route path="/recipe/details">
                <RecipeDetails />
              </Route>
              <Route exact path="/">
                <Redirect to="/recipe/list" />
              </Route>
            </Switch>
            <Footer />
          </div>
        </Router>
      </Provider>
    );
  }
}
export default App;
