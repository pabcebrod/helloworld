import * as React from 'react';
import { Link } from "react-router-dom";
import '../assets/scss/footer.scss'

interface IFooterProps {

}

interface IFooterState {

}

class Footer extends React.Component<IFooterProps, IFooterState> {
    constructor(props: IFooterProps) {
        super(props)
        //change state
    }

    public render() {
        return (
            <section className="complete-footer">
                <footer className="text-center text-white bg-light bottom">
                    <div className="container p-4 pb-0 ">
                        <section className="">
                            <p className="d-flex justify-content-center align-items-center">
                                <Link to='/about'>
                                    <button type="button" className="btn">
                                        About us
                                    </button>
                                </Link>
                                <Link to="/contact">
                                    <button type="button" className="btn">
                                        Contact us
                                    </button>
                                </Link>

                            </p>
                        </section>
                    </div>
                    <div className="text-center p-3 footer2" >
                        © 2021 Copyright:Recipe book
                    </div>
                </footer>
            </section>
        );
    }
}

export default Footer