import * as React from 'react';
import '../../../assets/scss/recipe.scss'
import { BrowserRouter as Router, Link } from "react-router-dom";
import { withHooksHOC } from '../../../hook/redirectionHook';
import { IRecipe } from '../../../assets/MockObjects';
import { Button } from '../../shared/Button';


interface IRecipeProps {
  recipes: any[];
  showRecipeDetails: (recipe: IRecipe) => void;
  handleRedirect: any;
}



class Recipe extends React.Component<IRecipeProps, {}>{

  constructor(props: IRecipeProps) {
    super(props)
    //change state

  }


  public onClick = (recipe: any) => {
    this.props.showRecipeDetails(recipe);
    this.props.handleRedirect("/recipe/list");
  }


  render() {



    const list = this.props.recipes.map((item: any, index: any) =>

      <div className="col-3 card">
        <img className="card-img-top" src={item.imgUrl} alt="Card image cap" />
        <div className="card-body">
          <h5 className="card-title" key={index}>{item.name}</h5>
          <h6 className="card-subtitle mb-2 text-muted">{item.tag}</h6>
          <Link to={`/recipe/details`} >

            <Button label={'Details'} size={"small"} onClick={() => this.onClick(item)} />

          </Link>

        </div>
      </div >

    );
    return (
      <div className="container d-flex justify-content-center">
        <div className="row">
          {this.props.recipes && list}
          {this.props.recipes.length === 0 &&
            <div className="auxText">
              <span>There is no recipe </span>
            </div>
          }
        </div>
      </div>
    );




  }
}

export default withHooksHOC(Recipe);