import * as React from 'react';
import '../../../assets/scss/main.scss'
import Recipes from '../../../containers/Recipe';
import { IRecipe, tags } from '../../../assets/MockObjects'
import Filter from '../../../containers/Filter';





interface IMainProps {
    recipes: IRecipe[]
    onFilterClick: (searchWord: string, searchTag: string) => void;
    onResetClick: () => void;
}

interface IMainState {
}


class Main extends React.Component<IMainProps, IMainState> {
    constructor(props: IMainProps) {
        super(props)
        //change state
    }

    public render() {

        return (
            <div className="main">
                <Filter tags={tags} onFilterClick={this.props.onFilterClick} onResetClick={this.props.onResetClick} />
                <br />
                <Recipes recipes={this.props.recipes} />
            </div>
        );
    }

}

export default Main


