import * as React from 'react';
import { Button } from '../../shared/Button';
import TextInput from '../../shared/TextInput';
import SelectInput from '../../shared/SelectInput';
import { withHooksHOC } from '../../../hook/storeHook';

interface IFilterProps {
    tags: string[]
    onFilterClick: (searchWord: string, searchTag: string) => void;
    onResetClick: () => void;
    searchWord: string
    searchTag: string
    getStore: any;
}


interface IFilterState {
    searchWordState: string
    searchTagState: string
    submitted: boolean


}
class Filter extends React.Component<IFilterProps, IFilterState> {

    constructor(props: IFilterProps) {
        super(props)
        //change state
        //let globalState = this.props.getStore();
        this.state = { searchWordState: this.props.getStore().searchWord, searchTagState: this.props.getStore().searchTag, submitted: false }
    }

    public onSearchWordTextChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({ searchWordState: event.target.value })
    }

    public onSearchTagTextChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({ searchTagState: event.target.value })
    }

    public onFilterClick = () => {
        this.props.onFilterClick(this.state.searchWordState, this.state.searchTagState)
    }

    public onResetClick = () => {
        this.props.onResetClick()
    }

    render() {
        return (
            <div className="container mx-auto mt-4">
                <form className="form-inline">
                    <div className="form-group mx-sm-3 mb-2">
                        <div className="row">
                            <TextInput label={"Word"} required={false} submitted={false} onChange={this.onSearchWordTextChange} type={"text"} placeholder={"Jam, Hamburguer..."} value={this.state.searchWordState} />
                            <SelectInput options={this.props.tags} onChange={this.onSearchTagTextChange} value={this.state.searchTagState} />
                            <div className="col-auto">
                                <Button label={'Filter'} size={"small"} onClick={this.onFilterClick} />
                            </div>
                            <div className="col-auto">
                                <Button label={'Show all'} size={"small"} onClick={this.onResetClick} />
                            </div>
                        </div>
                    </div>
                </form>
            </div >
        )
    }
}

export default withHooksHOC(Filter);