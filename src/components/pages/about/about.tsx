import * as React from 'react';




class AboutUs extends React.Component<{}, {}>{

    render() {

        return (
            <div className="container py-2 filter rounded content">
                <div className="row justify-content-center">
                    <div className="row gy-2 gx-3 align-items-center col-auto postcard">
                        <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi eu turpis vel enim suscipit hendrerit.
                            Cras condimentum nisi in risus luctus vulputate. Nulla condimentum tincidunt dui, ac euismod dui tincidunt ac.
                            Ut convallis laoreet ligula. Pellentesque nunc augue, suscipit sed viverra sed, ullamcorper at diam.
                            Maecenas efficitur massa placerat elit tincidunt vulputate. Aenean nec feugiat tortor, id rhoncus nunc.
                            Aenean ornare eget enim eu consequat. Suspendisse id erat dignissim, semper erat et, posuere leo.
                            Vivamus fermentum est eu sapien hendrerit, in egestas quam volutpat.
                            Nullam maximus condimentum nisl, ac rutrum urna vulputate eu.
                            Pellentesque eleifend lectus et justo fermentum sollicitudin.
                            In commodo odio eget risus accumsan, id laoreet erat accumsan.
                            Praesent facilisis consectetur vehicula.
                            Vivamus sit amet pharetra tellus, quis pretium lacus.
                            Nulla non pretium neque.</span>
                    </div>
                </div>
            </div>
        )
    }
}

export default AboutUs;