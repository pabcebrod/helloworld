import * as React from 'react';
import TextInput from '../../shared/TextInput';
import TextareaInput from '../../shared/TextareaInput';
import { Button } from '../../shared/Button';
import { withHooksHOC } from '../../../hook/redirectionHook';


interface IContactProps {
    handleRedirect: any;
}

interface IContactState {
    email: string;
    message: string;
    submmited: boolean;
}


class Contact extends React.Component<IContactProps, IContactState>{
    constructor(props: IContactProps) {
        super(props)
        //change state
        this.state = { email: "", message: "", submmited: false }
    }

    public onChangeEmail = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({ email: event.target.value })
    }

    public onChangeMessage = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({ message: event.target.value })
    }

    public onClick = () => {
        this.setState({ submmited: true })

        if (this.state.email != "" && this.state.message != "") {
            this.props.handleRedirect("/recipe/list");
        }
    }


    render() {

        return (
            <div>


                <div id="createRecipeForm" className="contactForm container py-2 filter rounded content">
                    <h2>Contact us with a message</h2>
                    <div className="row justify-content-center">
                        <form className="row gy-2 gx-3 align-items-center col-auto postcard">
                            <div className="mb-3">
                                <label className="form-label">Email</label>
                                <TextInput label={"Email"} type={"text"} required={true} submitted={this.state.submmited} onChange={this.onChangeEmail} placeholder={""} />
                            </div>
                            <div className="mb-3">
                                <label className="form-label">Comment</label>
                                <TextareaInput label={"Comment"} type={"text"} required={true} submitted={this.state.submmited} onChange={this.onChangeMessage} />
                            </div>
                            <div className="mb-3">
                                <Button primary={true} size={"medium"} onClick={this.onClick} label={"Send message"} />
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

export default withHooksHOC(Contact);