import * as React from 'react';
import TextInput from '../../shared/TextInput';
import SelectInput from '../../shared/SelectInput';
import TextareaInput from '../../shared/TextareaInput';
import '../../../assets/scss/form.scss'
import { Button } from '../../shared/Button';
import { withHooksHOC } from '../../../hook/redirectionHook';
import { IRecipe } from '../../../assets/MockObjects';


interface IRecipeFormProps {
  tags: string[]
  handleRedirect: any;
  addRecipe: (recipe: IRecipe) => void;
}

interface IRecipeFormState {
  submitted: boolean;
  name: string;
  ingredients: string;
  steps: string;
  tag: string;
  imgUrl: string;
}




class RecipeForm extends React.Component<IRecipeFormProps, IRecipeFormState>{

  constructor(props: IRecipeFormProps) {
    super(props)
    //change state

    this.state = { name: "", ingredients: "", steps: "", tag: "", imgUrl: "", submitted: false }
  }

  public onChangeName = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({ name: event.target.value })
  }

  public onChangeIngredients = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({ ingredients: event.target.value })
  }

  public onChangeSteps = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({ steps: event.target.value })
  }

  public onChangeTags = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({ tag: event.target.value })
  }

  public onChangeImageUrl = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({ imgUrl: event.target.value })
  }

  public onClick = () => {
    this.setState({ submitted: true })
    if (this.state.imgUrl && this.state.ingredients && this.state.name && this.state.tag && this.state.steps) {
      let recipe: IRecipe = {
        name: this.state.name,
        steps: this.state.steps,
        ingredients: this.state.ingredients,
        tag: this.state.tag,
        imgUrl: this.state.imgUrl,
      }
      this.props.addRecipe(recipe)
      this.props.handleRedirect("/recipe/list");
    }
  }

  public onClickCancel = () => {

    this.props.handleRedirect("/recipe/list");
  }

  render() {

    return (
      <div className="container">


        <div className="createForm container py-2 filter rounded content">
          <h2>Recipe form</h2>
          <div className="row justify-content-center">
            <form className="row gy-2 gx-3 align-items-center col-auto postcard">
              <div className="mb-3">
                <label className="form-label">Recipe name</label>
                <TextInput label={"Name"} type={"text"} required={true} submitted={this.state.submitted} onChange={this.onChangeName} placeholder={""} />
              </div>
              <div className="mb-3">
                <label className="form-label">Ingredients</label>
                <TextareaInput label={"Ingredients"} type={"text"} required={true} submitted={this.state.submitted} onChange={this.onChangeIngredients} />
              </div>
              <div className="mb-3">
                <label className="form-label">Steps</label>
                <TextareaInput label={"Ingredients"} type={"text"} required={true} submitted={this.state.submitted} onChange={this.onChangeSteps} />
              </div>
              <div className="mb-3">
                <label className="form-label">Image url</label>
                <TextInput label={"imgUrl"} type={"text"} required={true} submitted={this.state.submitted} onChange={this.onChangeImageUrl} placeholder={""} />
              </div>
              <div className="mb-3">
                <label className="form-label">Tag</label>
                <SelectInput options={this.props.tags} onChange={this.onChangeTags} submitted={this.state.submitted} required={true} />
              </div>
              <div className="mb-3">
                <Button primary={true} size={"medium"} onClick={this.onClick} label={"Create"} />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <Button primary={true} size={"medium"} onClick={this.onClickCancel} label={"Cancel"} backgroundColor={"red"} />
              </div>
            </form>
          </div>
        </div>
      </div>
    )
  }
}

export default withHooksHOC(RecipeForm);