import * as React from 'react';
import { Link } from 'react-router-dom';
import '../../../assets/scss/form.scss'
import { withHooksHOC } from '../../../hook/storeHook';
import { Button } from '../../shared/Button';


export interface IRecipe {
  name: string;
  ingredients: string;
  steps: string;
  tag: string;
  imgUrl: string;
}

interface IRecipeDetailsProps {
  getStore: any;
}

interface IRecipeDetailsState {
  recipe: IRecipe;

}

class RecipeDetails extends React.Component<IRecipeDetailsProps, IRecipeDetailsState>{
  constructor(props: IRecipeDetailsProps) {
    super(props)
    //change state
    this.state = { recipe: this.props.getStore().recipeDetailed }
  }

  render() {

    return (
      <div className="showRecipeCar container py-2 filter rounded content">
        <div className="row justify-content-center">
          <div className="row gy-2 gx-3 align-items-center col-auto postcard">
            <div className="card mb-3">
              <img className="card-img-top" src={this.state.recipe.imgUrl} alt="Card image cap" />
              <div className="card-body">
                <h4 className="card-title">{this.state.recipe.name}</h4>
                <p className="card-text"><small className="text-muted">{this.state.recipe.tag}</small></p>
                <h6 className="card-title">Ingredients:</h6>
                <p className="card-text">{this.state.recipe.ingredients}</p>
                <h6 className="card-title">Steps:</h6>
                <p className="card-text">{this.state.recipe.steps}</p>
                <Link to="/recipe/list">
                  <Button primary={true} size={"medium"} label={"Back"} />
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div >
    )
  }
}

export default withHooksHOC(RecipeDetails);
