import * as React from 'react';
import { BrowserRouter as Router, Link } from "react-router-dom";

interface IHeaderProps {

}

interface IHeaderState {

}

class Header extends React.Component<IHeaderProps, IHeaderState> {
    constructor(props: IHeaderProps) {
        super(props)
        //change state
    }

    public render() {
        return (
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
                <div className="container-fluid">
                    <img src="https://cdn-icons-png.flaticon.com/512/683/683488.png" alt="MDN" width="80" height="100" />

                    <Link to="/recipe/list">
                        <a className="navbar-brand">Recipe book</a>
                    </Link>


                    {/*    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                                <span className="navbar-toggler-icon"></span>
                            </button> */}


                    <div className="collapse navbar-collapse" id="navbarText">
                        <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                            <li className="nav-item">

                                <Link to="/recipe/create">
                                    <a className="nav-link">How to create recipe</a>
                                </Link>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>

        );
    }

}

export default Header