import React from 'react';

interface TextInputProps {

  label: string;
  type: string;
  required: boolean;
  submitted: boolean;

  onChange: (event: any) => void;

}


interface TextInputState {
  value: string;
  validation: string;
}


class TextInput extends React.Component<TextInputProps, TextInputState>{

  constructor(props: TextInputProps) {
    super(props);

    this.state = {
      value: '',
      validation: ''
    }


  }

  onChange = (event: any) => {
    this.setState({ value: event.target.value });
    this.props.onChange(event);
  }


  validate = () => {
    if (this.props.required && !this.state.value) {
      this.setState({ validation: 'is-invalid' });
      return
    }

    this.setState({ validation: '' });
  }



  public render() {
    return (
      <div className="col-auto">
        <textarea className={`form-control ${this.props.submitted && this.state.value === '' ? "is-invalid" : ""} ${this.state.validation}`} onBlur={this.validate} id="autoSizingInput" onChange={this.onChange} required={this.props.required} />
        <div className="invalid-feedback">
          This field is obligatory.
        </div>
      </div>
    );
  }
}

export default TextInput;

