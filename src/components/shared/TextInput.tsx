import React from 'react';


interface TextInputState {
  value: string;
  submitted: boolean;
  validation: string
}

interface TextInputProps {

  label: string;
  type: string;
  required: boolean;
  submitted: boolean;
  placeholder: string;
  onChange: (event: any) => void;
  value?: string;

}

class TextInput extends React.Component<TextInputProps, TextInputState>{

  constructor(props: TextInputProps) {
    super(props);

    this.state = {
      value: this.props.value ? this.props.value : "",
      submitted: this.props.submitted,
      validation: ''
    }
  }

  onChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({ value: event.target.value });
    this.props.onChange(event);
  }


  validate = () => {
    if (this.props.required && !this.state.value) {
      this.setState({ validation: 'is-invalid' });
      return
    }

    this.setState({ validation: '' });
  }



  public render() {
    return (
      <div className="col-auto">
        <input value={this.state.value} type={this.props.type} className={`form-control ${this.props.submitted && this.state.value === '' ? "is-invalid" : ""} ${this.state.validation}`} id="autoSizingInput" onChange={this.onChange} onBlur={this.validate} required={this.props.required} placeholder={this.props.placeholder} />
        <div className="invalid-feedback">
          This field is obligatory.
        </div>
      </div>
    );
  }
}

export default TextInput;

