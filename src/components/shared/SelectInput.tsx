import React from 'react';


interface SelectInputState {
  value: string;
  submitted: boolean;
  validation: string

}

interface SelectInputProps {
  options: string[];
  submitted?: boolean;
  required?: boolean;
  onChange: (event: any) => void;
  value?: string

}

class TextInput extends React.Component<SelectInputProps, SelectInputState>{

  constructor(props: SelectInputProps) {
    super(props);
    //state change
    this.state = {
      value: this.props.value ? this.props.value : "",
      submitted: this.props.submitted ? this.props.submitted : false,
      validation: ''
    }
  }

  onChange = (event: any) => {
    this.setState({ value: event.target.value });
    this.props.onChange(event);
  }

  validate = () => {
    if (this.props.required && !this.state.value) {
      this.setState({ validation: 'is-invalid' });
      return
    }

    this.setState({ validation: '' });
  }


  public render() {
    return (
      <div className="col-auto">
        <select value={this.state.value} required={this.props.required} className={`form-control ${this.props.submitted && this.state.value === '' ? "is-invalid" : ""} ${this.state.validation}`} id="autoSizingInput" onChange={this.onChange}>
          <option value={""} selected>Choose here</option>
          {this.props.options.map((option) => (
            <option value={option}>{option}</option>
          ))}
        </select>
        <div className="invalid-feedback">
          This field is obligatory.
        </div>
      </div>
    );
  }
}

export default TextInput;

