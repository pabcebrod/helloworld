import { IRecipe, recipeArray } from '../assets/MockObjects'

interface IGlobalState {
    recipes: IRecipe[];
    recipesBackup: IRecipe[];
    searchWord: string;
    searchTag: string;
    recipeDetailed: IRecipe;
}

export default IGlobalState;

export const initialState: IGlobalState = {
    recipes: Object.assign([], recipeArray),
    searchWord: "",
    searchTag: "",
    recipesBackup: Object.assign([], recipeArray),
    recipeDetailed: { name: "", ingredients: "", steps: "", imgUrl: "", tag: "" },
}