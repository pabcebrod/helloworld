import { useStore } from "react-redux";

export const withHooksHOC = (Component: any) => {
    return (props: any) => {
        const store = useStore();
        const getStore = () => {
            return store.getState()
        }

        return <Component getStore={getStore} {...props} />;
    };
};