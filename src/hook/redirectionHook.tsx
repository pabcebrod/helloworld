import { useHistory } from "react-router-dom";

export const withHooksHOC = (Component: any) => {
    return (props: any) => {
        const history = useHistory();
        const handleRedirect = (path: string) => {
            history.push(path)
        }

        return <Component handleRedirect={handleRedirect} {...props} />;
    };
};