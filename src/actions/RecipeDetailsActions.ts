import { Action } from 'redux'
import { IRecipe } from '../assets/MockObjects';



export interface IRecipeDetailsAction extends Action {
    recipe: IRecipe,
}

export enum RecipeDetailsAction {
    SHOW_RECIPE = "SHOW_RECIPE"
}