import { Action } from 'redux'



export interface IFilterAction extends Action {
    searchWord: string;
    searchTag: string;
}

export enum MainActions {
    MAIN_FILTER = "MAIN_FILTER",
    MAIN_RESET = "MAIN_RESET"
}