import { Action } from 'redux'
import { IRecipe } from '../assets/MockObjects';



export interface IRecipeFormAction extends Action {
    recipe: IRecipe,
}

export enum RecipeFormActions {
    CREATE_RECIPE = "CREATE_RECIPER"
}