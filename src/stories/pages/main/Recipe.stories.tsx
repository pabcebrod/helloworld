import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';

import Recipe from '../../../components/pages/main/Recipe';

export default {
    title: 'pages/main/Recipe',
    component: Recipe,
} as ComponentMeta<typeof Recipe>;

const recipeArray = [
    {
        name: "Recipe1",
        ingredients: " 300 gramos de jamón Joselito, 2 litros de leche entera,100 gramos de mantequilla de oveja,110 gramos de harina, sal,pimienta, 200 gramos de leche de oveja",
        steps: "Cortar el jamón en brunoise muy fino."
            + "Reservar. Verter la leche entera en una olla a calentar para tenerla lista en el momento de añadirla al roux, estando caliente empezará a hervir antes."
            + "En una cazuela al fuego, derretir la mantequilla y retirar del fuego. Añadir el jamón cortado en brunoise y mezclar. Incorporar la harina y tostar."
            + "Cuando la harina esté tostada, añadir la mitad de la leche entera, cuando se integre con las varillas verter el resto y volver a batir con las varillas hasta conseguir una mezcla homogénea.",
        tag: "vegetarian",
        imgUrl: "https://cocina-casera.com/wp-content/uploads/2015/08/mejores-recetas-cocina-casera.jpg"
    },

    {
        name: "Recipe2",
        ingredients: " 300 gramos de jamón Joselito, 2 litros de leche entera,100 gramos de mantequilla de oveja,110 gramos de harina, sal,pimienta, 200 gramos de leche de oveja",
        steps: "Cortar el jamón en brunoise muy fino."
            + "Reservar. Verter la leche entera en una olla a calentar para tenerla lista en el momento de añadirla al roux, estando caliente empezará a hervir antes."
            + "En una cazuela al fuego, derretir la mantequilla y retirar del fuego. Añadir el jamón cortado en brunoise y mezclar. Incorporar la harina y tostar."
            + "Cuando la harina esté tostada, añadir la mitad de la leche entera, cuando se integre con las varillas verter el resto y volver a batir con las varillas hasta conseguir una mezcla homogénea.",
        tag: "vegan",
        imgUrl: "https://cocina-casera.com/wp-content/uploads/2015/08/mejores-recetas-cocina-casera.jpg"
    },

    {
        name: "Recipe3",
        ingredients: " 300 gramos de jamón Joselito, 2 litros de leche entera,100 gramos de mantequilla de oveja,110 gramos de harina, sal,pimienta, 200 gramos de leche de oveja",
        steps: "Cortar el jamón en brunoise muy fino."
            + "Reservar. Verter la leche entera en una olla a calentar para tenerla lista en el momento de añadirla al roux, estando caliente empezará a hervir antes."
            + "En una cazuela al fuego, derretir la mantequilla y retirar del fuego. Añadir el jamón cortado en brunoise y mezclar. Incorporar la harina y tostar."
            + "Cuando la harina esté tostada, añadir la mitad de la leche entera, cuando se integre con las varillas verter el resto y volver a batir con las varillas hasta conseguir una mezcla homogénea.",
        tag: "omnivore",
        imgUrl: "https://cocina-casera.com/wp-content/uploads/2015/08/mejores-recetas-cocina-casera.jpg"
    },
    {
        name: "Recipe4",
        ingredients: " 300 gramos de jamón Joselito, 2 litros de leche entera,100 gramos de mantequilla de oveja,110 gramos de harina, sal,pimienta, 200 gramos de leche de oveja",
        steps: "Cortar el jamón en brunoise muy fino."
            + "Reservar. Verter la leche entera en una olla a calentar para tenerla lista en el momento de añadirla al roux, estando caliente empezará a hervir antes."
            + "En una cazuela al fuego, derretir la mantequilla y retirar del fuego. Añadir el jamón cortado en brunoise y mezclar. Incorporar la harina y tostar."
            + "Cuando la harina esté tostada, añadir la mitad de la leche entera, cuando se integre con las varillas verter el resto y volver a batir con las varillas hasta conseguir una mezcla homogénea.",
        tag: "omnivore",
        imgUrl: "https://cocina-casera.com/wp-content/uploads/2015/08/mejores-recetas-cocina-casera.jpg"
    },
];
const Template: ComponentStory<typeof Recipe> = (args) => <Recipe {...args} />;

export const BaseRecipe = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
BaseRecipe.args = {
    recipes: recipeArray,
};