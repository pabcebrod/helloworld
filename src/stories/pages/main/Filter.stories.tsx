import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';

import Filter from '../../../components/pages/main/Filter';

export default {
    title: 'pages/main/Filter',
    component: Filter,
} as ComponentMeta<typeof Filter>;

const tags = [
    "vegetarian",
    "vegan",
    "omnivore"
]
const Template: ComponentStory<typeof Filter> = (args) => <Filter {...args} />;

export const BaseFilter = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
BaseFilter.args = {
    tags: tags,
};
