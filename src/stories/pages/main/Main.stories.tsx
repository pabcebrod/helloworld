import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';

import Main from '../../../components/pages/main/Main';

export default {
    title: 'pages/main/Main',
    component: Main,
} as ComponentMeta<typeof Main>;

const tags = [
    "vegetarian",
    "vegan",
    "omnivore"
]
const Template: ComponentStory<typeof Main> = (args) => <Main {...args} />;

export const BaseMain = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
BaseMain.args = {};
