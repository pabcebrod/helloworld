import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';

import Contact from '../../../components/pages/contact/Contact';

export default {
    title: 'pages/contact/Contact',
    component: Contact,
} as ComponentMeta<typeof Contact>;

const tags = [
    "vegetarian",
    "vegan",
    "omnivore"
]
const Template: ComponentStory<typeof Contact> = (args) => <Contact {...args} />;

export const BaseContact = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
BaseContact.args = {

};
