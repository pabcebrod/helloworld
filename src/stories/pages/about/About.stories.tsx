import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';

import About from '../../../components/pages/about/about';

export default {
    title: 'pages/about/About',
    component: About,
} as ComponentMeta<typeof About>;

const tags = [
    "vegetarian",
    "vegan",
    "omnivore"
]
const Template: ComponentStory<typeof About> = (args) => <About {...args} />;

export const BaseAbout = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
BaseAbout.args = {

};
