import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';

import RecipeForm from '../../../components/pages/create-recipe/RecipeForm';

export default {
    title: 'pages/create-recipe/RecipeForm',
    component: RecipeForm,
} as ComponentMeta<typeof RecipeForm>;

const tags = [
    "vegetarian",
    "vegan",
    "omnivore"
]
const Template: ComponentStory<typeof RecipeForm> = (args) => <RecipeForm {...args} />;

export const BaseRecipeForm = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
BaseRecipeForm.args = {
    tags: tags,
};
