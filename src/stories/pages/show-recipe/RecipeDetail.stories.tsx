import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';

import RecipeDetail from '../../../components/pages/show-recipe/recipeDetail';
import { recipeArray } from '../../../assets/MockObjects'

export default {
    title: 'pages/show-recipe/RecipeDetail',
    component: RecipeDetail,
} as ComponentMeta<typeof RecipeDetail>;

const Template: ComponentStory<typeof RecipeDetail> = (args) => <RecipeDetail {...args} />;

export const BaseRecipeDetail = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
BaseRecipeDetail.args = {
    recipe: recipeArray[0],

};


