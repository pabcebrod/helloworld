import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';

import SelectInput from '../../components/shared/SelectInput';

export default {
    title: 'shared/SelectInput',
    component: SelectInput,
} as ComponentMeta<typeof SelectInput>;

const tags = [
    "vegetarian",
    "vegan",
    "omnivore"
]

const Template: ComponentStory<typeof SelectInput> = (args) => <SelectInput {...args} />;

export const EatingHabits = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
EatingHabits.args = {
    options: tags,
};
