import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';

import TextInput from '../../components/shared/TextInput';

export default {
    title: 'shared/TextInput',
    component: TextInput,
} as ComponentMeta<typeof TextInput>;


const Template: ComponentStory<typeof TextInput> = (args) => <TextInput {...args} />;

export const BaseTextInput = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
BaseTextInput.args = {
    label: "textAreaExample",
    type: "text",
    placeholder: "This is a placeholder",
    required: true,
    submitted: false,
};
