import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';

import { Button } from '../../components/shared/Button';

export default {
  title: 'shared/Button',
  component: Button,
  argTypes: {
    backgroundColor: { control: 'color' },
  },
} as ComponentMeta<typeof Button>;

const Template: ComponentStory<typeof Button> = (args) => <Button {...args} />;

export const CreateButton = Template.bind({});
CreateButton.args = {
  primary: true,
  label: 'Create',
  size: "medium"
};

export const FilterButton = Template.bind({});
FilterButton.args = {
  size: "small",
  label: 'Filter',
};

