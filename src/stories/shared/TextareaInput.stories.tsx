import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';

import TextareaInput from '../../components/shared/TextareaInput';

export default {
    title: 'shared/TextareaInput',
    component: TextareaInput,
} as ComponentMeta<typeof TextareaInput>;


const Template: ComponentStory<typeof TextareaInput> = (args) => <TextareaInput {...args} />;

export const BaseTextAreaInput = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
BaseTextAreaInput.args = {
    label: "textAreaExample",
    type: "text",
    required: true,
    submitted: false,
};
