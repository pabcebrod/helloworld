export function filterList(recipes: IRecipe[], searchWord: string, searchTag: string): IRecipe[] {

    if (searchTag != "") {
        recipes = recipes.filter(recipe => recipe.tag === searchTag)
    }

    if (searchWord != "") {
        recipes = recipes.filter(recipe => recipe.name.includes(searchWord) || recipe.ingredients.includes(searchWord));
    }

    return recipes;
}

export interface IRecipe {
    name: string;
    ingredients: string;
    steps: string;
    tag: string;
    imgUrl: string;
}

export const tags = [
    "vegetarian",
    "vegan",
    "omnivore"
]

export const recipeArray = [
    {
        name: "Hamburguesa de falafel",
        ingredients: " 300 gramos de jamón Joselito, 2 litros de leche entera,100 gramos de mantequilla de oveja,110 gramos de harina, sal,pimienta, 200 gramos de leche de oveja",
        steps: "Cortar el jamón en brunoise muy fino."
            + "Reservar. Verter la leche entera en una olla a calentar para tenerla lista en el momento de añadirla al roux, estando caliente empezará a hervir antes."
            + "En una cazuela al fuego, derretir la mantequilla y retirar del fuego. Añadir el jamón cortado en brunoise y mezclar. Incorporar la harina y tostar."
            + "Cuando la harina esté tostada, añadir la mitad de la leche entera, cuando se integre con las varillas verter el resto y volver a batir con las varillas hasta conseguir una mezcla homogénea.",
        tag: "vegetarian",
        imgUrl: "https://cocina-casera.com/wp-content/uploads/2015/08/mejores-recetas-cocina-casera.jpg"
    },

    {
        name: "Falafel",
        ingredients: " 300 gramos de jamón Joselito, 2 litros de leche entera,100 gramos de mantequilla de oveja,110 gramos de harina, sal,pimienta, 200 gramos de leche de oveja",
        steps: "Cortar el jamón en brunoise muy fino."
            + "Reservar. Verter la leche entera en una olla a calentar para tenerla lista en el momento de añadirla al roux, estando caliente empezará a hervir antes."
            + "En una cazuela al fuego, derretir la mantequilla y retirar del fuego. Añadir el jamón cortado en brunoise y mezclar. Incorporar la harina y tostar."
            + "Cuando la harina esté tostada, añadir la mitad de la leche entera, cuando se integre con las varillas verter el resto y volver a batir con las varillas hasta conseguir una mezcla homogénea.",
        tag: "vegan",
        imgUrl: "https://cocina-casera.com/wp-content/uploads/2015/08/mejores-recetas-cocina-casera.jpg"
    },

    {
        name: "Croquetas del puchero",
        ingredients: " 300 gramos de jamón Joselito, 2 litros de leche entera,100 gramos de mantequilla de oveja,110 gramos de harina, sal,pimienta, 200 gramos de leche de oveja",
        steps: "Cortar el jamón en brunoise muy fino."
            + "Reservar. Verter la leche entera en una olla a calentar para tenerla lista en el momento de añadirla al roux, estando caliente empezará a hervir antes."
            + "En una cazuela al fuego, derretir la mantequilla y retirar del fuego. Añadir el jamón cortado en brunoise y mezclar. Incorporar la harina y tostar."
            + "Cuando la harina esté tostada, añadir la mitad de la leche entera, cuando se integre con las varillas verter el resto y volver a batir con las varillas hasta conseguir una mezcla homogénea.",
        tag: "omnivore",
        imgUrl: "https://cocina-casera.com/wp-content/uploads/2015/08/mejores-recetas-cocina-casera.jpg"
    },
    {
        name: "Croquetas del puchero",
        ingredients: " 300 gramos de jamón Joselito, 2 litros de leche entera,100 gramos de mantequilla de oveja,110 gramos de harina, sal,pimienta, 200 gramos de leche de oveja",
        steps: "Cortar el jamón en brunoise muy fino."
            + "Reservar. Verter la leche entera en una olla a calentar para tenerla lista en el momento de añadirla al roux, estando caliente empezará a hervir antes."
            + "En una cazuela al fuego, derretir la mantequilla y retirar del fuego. Añadir el jamón cortado en brunoise y mezclar. Incorporar la harina y tostar."
            + "Cuando la harina esté tostada, añadir la mitad de la leche entera, cuando se integre con las varillas verter el resto y volver a batir con las varillas hasta conseguir una mezcla homogénea.",
        tag: "omnivore",
        imgUrl: "https://cocina-casera.com/wp-content/uploads/2015/08/mejores-recetas-cocina-casera.jpg"
    },

    {
        name: "Falafel",
        ingredients: " 300 gramos de jamón Joselito, 2 litros de leche entera,100 gramos de mantequilla de oveja,110 gramos de harina, sal,pimienta, 200 gramos de leche de oveja",
        steps: "Cortar el jamón en brunoise muy fino."
            + "Reservar. Verter la leche entera en una olla a calentar para tenerla lista en el momento de añadirla al roux, estando caliente empezará a hervir antes."
            + "En una cazuela al fuego, derretir la mantequilla y retirar del fuego. Añadir el jamón cortado en brunoise y mezclar. Incorporar la harina y tostar."
            + "Cuando la harina esté tostada, añadir la mitad de la leche entera, cuando se integre con las varillas verter el resto y volver a batir con las varillas hasta conseguir una mezcla homogénea.",
        tag: "vegan",
        imgUrl: "https://cocina-casera.com/wp-content/uploads/2015/08/mejores-recetas-cocina-casera.jpg"
    },

    {
        name: "Hamburguesa de falafel",
        ingredients: " 300 gramos de jamón Joselito, 2 litros de leche entera,100 gramos de mantequilla de oveja,110 gramos de harina, sal,pimienta, 200 gramos de leche de oveja",
        steps: "Cortar el jamón en brunoise muy fino."
            + "Reservar. Verter la leche entera en una olla a calentar para tenerla lista en el momento de añadirla al roux, estando caliente empezará a hervir antes."
            + "En una cazuela al fuego, derretir la mantequilla y retirar del fuego. Añadir el jamón cortado en brunoise y mezclar. Incorporar la harina y tostar."
            + "Cuando la harina esté tostada, añadir la mitad de la leche entera, cuando se integre con las varillas verter el resto y volver a batir con las varillas hasta conseguir una mezcla homogénea.",
        tag: "vegetarian",
        imgUrl: "https://cocina-casera.com/wp-content/uploads/2015/08/mejores-recetas-cocina-casera.jpg"
    },
];

export const recipeArray2 = [
    {
        name: "Recipe1",
        ingredients: "Recipe2",
        steps: "Recipe1",
        tag: "vegan",
        imgUrl: "https://cocina-casera.com/wp-content/uploads/2015/08/mejores-recetas-cocina-casera.jpg"
    },

    {
        name: "Recipe3",
        ingredients: "Recipe3",
        steps: "Recipe3",
        tag: "omnivore",
        imgUrl: "https://cocina-casera.com/wp-content/uploads/2015/08/mejores-recetas-cocina-casera.jpg"
    },
];


